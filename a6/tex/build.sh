#!/bin/sh


case $1 in
    clean)
#        rm -fr */*.log */*.aux */*.dvi */*.bbl */*.blg */*.ilg */*.toc */*.lof */*.lot */*.idx */*.ind */*.ps  */*~ */*.lol */*.nlo */*.nls */*.out
#        rm -fr */**/*.log */**/*.aux */**/*.dvi */**/*.bbl */**/*.blg */**/*.ilg */**/*.toc */**/*.lof */**/*.lot */**/*.idx */**/*.ind */**/*.ps  */**/*~ */**/*.lol */**/*.nlo */**/*.nls */**/*.out
#        rm -f *.log *.aux *.dvi *.bbl *.blg *.ilg *.toc *.lof *.lot *.idx *.ind *.ps  *~ *.lol *.nlo *.nls *.out
	rm -f `find . -iname '*.aux'` `find . -iname '*.log'` `find . -iname '*.dvi'` `find . -iname '*.bbl'` `find . -iname '*.blg'` `find . -iname '*.ilg'` `find . -iname '*.toc'` `find . -iname '*.lof'` `find . -iname '*.lot'` `find . -iname '*.idx'` `find . -iname '*.ind'` `find . -iname '*.ps'` `find . -iname '*~'` `find . -iname '*.lol'` `find . -iname '*.nlo'` `find . -iname '*.nls'` `find . -iname '*.out'` `find . -iname '*.nav'` `find . -iname '*.snm'` `find . -iname '*.vrb'`
	rm -f A6.run.xml A6-blx.bib
        echo cleaning done.
        ;;
    *)
        pdflatex -interaction=nonstopmode --shell-escape A6
        bibtex8 -H A6
        makeindex A6.nlo -s nomencl.ist -o A6.nls
        pdflatex -interaction=nonstopmode --shell-escape A6
        pdflatex -interaction=nonstopmode --shell-escape A6
        echo building done.
        ;;
esac
